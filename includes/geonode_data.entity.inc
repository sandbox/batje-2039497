<?php

/**
 * The class used for Geonode entities
 */
class GeonodeData extends Entity {

  public function __construct($values = array()) {
    parent::__construct($values, 'geonode_data');
  }

  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return array('path' => 'geonode/' . $this->id);
  }


}



class GeonodeDataController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }


  /**
   * Create a geonode data - we first set up the values that are specific
   * to our model schema but then also go through the EntityAPIController
   * function.
   *
   * @param $type
   *   The machine-readable type of the model.
   *
   * @return
   *   A model object with all default fields initialized.
   */
  public function create(array $values = array()) {
    global $user;
    // Add values that are specific to our Model
    $values += array(
        'id' => '',
        'is_new' => TRUE,
        'created' => REQUEST_TIME,
        'changed' => REQUEST_TIME,
        'uuid' => '',
        'uid' => $user->uid,
        'active' => TRUE,
        'terms' => Array(),
    );

    $model = parent::create($values);
    return $model;
  }


  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('geonode_data', $entity);
    $content['author'] = array('#markup' => t('Created by: !author', array('!author' => $wrapper->uid->name->value(array('sanitize' => TRUE)))));

    // Make Abstract and Status themed like default fields.
    $content['abstract'] = array(
        '#theme' => 'field',
        '#weight' => 0,
        '#title' =>t('Abstract'),
        '#access' => TRUE,
        '#label_display' => 'above',
        '#view_mode' => 'full',
        '#language' => LANGUAGE_NONE,
        '#field_name' => 'field_fake_abstract',
        '#field_type' => 'text',
        '#entity_type' => 'geonode_data',
        '#bundle' => 'geonode_data',
        '#items' => array(array('value' => $entity->abstract)),
        '#formatter' => 'text_default',
        0 => array('#markup' => check_plain($entity->abstract))
    );

    openlayers_include();
    ctools_include('dependent');
//    drupal_add_js(drupal_get_path('module', 'openlayers_ui') .
//        '/js/openlayers_ui.maps.js');
//    drupal_add_css(drupal_get_path('module', 'openlayers_ui') .
//        '/openlayers_ui.css');

    // Pass variables etc. to javascript
    $pass_values = array(
        'openlayersForm' => array(
            'projectionLayers' => openlayers_ui_get_projection_options(),
        ),
    );
    drupal_add_js($pass_values, 'setting');



    // centerpoint & zoom of this map are overridden
    // by the mapformvalues behavior on page load

    $resolution = $entity->field_geonode_data_bb[LANGUAGE_NONE][0]['right'] - $entity->field_geonode_data_bb[LANGUAGE_NONE][0]['left'];
    $width = 400; // in pixels
    $zoom = 7 - round( 7 * ($resolution/360));

    $maps = openlayers_maps(TRUE);
    $mapclass = $maps[variable_get('geonode_default_map', variable_get('openlayers_default_map', 'default'))];
    $map = $mapclass->data;

        $map['id'] = 'openlayers-center-' . $entity->uuid;
        $map['center']['initial']['centerpoint'] = $entity->field_geonode_data_bb[LANGUAGE_NONE][0]['lon'] . "," . $entity->field_geonode_data_bb[LANGUAGE_NONE][0]['lat'];
        $map['center']['initial']['zoom'] = $zoom;
        $map['layers'][$entity->uuid] = $entity->uuid;
        $map['layer_weight'][] = array ($entity->uuid => 1 );
        if (isset($map['layer_switcher'])) {
          $map['layer_switcher'][$entity->uuid] = $entity->uuid;
        }
        if (isset($map['layer_activated'])) {
          $map['layer_activated'][$entity->uuid] = $entity->uuid;
        }
        foreach($map['layer_styles'] as $key=>$value) {
          if (($value == 0) && ($map['layer_activated'][$key] == 0)) {
            unset($map['layer_switcher'][$key]);
            unset($map['layer_activated'][$key]);
            unset($map['layer_styles'][$key]);
            unset($map['layer_styles_select'][$key]);
            unset($map['layer_weight'][$key]);
          }
        }
    $content['map'] = array( 0 => array('#markup' =>  openlayers_render_map_data($map)));
    // Make Abstract and Status themed like default fields.
/*    $content['title'] = array(
        '#theme' => 'field',
        '#weight' => 0,
        '#title' =>t('Title'),
        '#access' => TRUE,
        '#label_display' => 'above',
        '#view_mode' => 'full',
        '#language' => LANGUAGE_NONE,
        '#field_name' => 'field_fake_title',
        '#field_type' => 'text',
        '#entity_type' => 'geonode_data',
        '#bundle' => 'geonode_data',
        '#items' => array(array('value' => $entity->title)),
        '#formatter' => 'text_default',
        0 => array('#markup' => check_plain($entity->title))
    );
*/

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

}

/**
 * UI controller for Task Type.
 */
class GeonodeDataTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Geonode Data Layers.';
    return $items;
    // The page to view our entities - needs to follow what
    // is defined in basic_uri and will use load_basic to retrieve
    // the necessary entity info.
/*    $items['geonode/dataset/%geonode_data'] = array(
        'page callback' => 'geonode_dataset_view',
        'module' => 'geonode',
        'file path' => drupal_get_path('module', 'geonode'),
        'file' => 'geonode.pages.inc',
        'page arguments' => array(2),
        'access arguments' => array('view any geonode_dataset'),
    );
*/
    watchdog("geonode", "returning the menu");
  }
}