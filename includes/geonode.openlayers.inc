<?php
/**
 * Implements hook_openlayers_layers().
 */
function geonode_get_my_openlayers_layers() {
  /*
   *  Find all active layers
   */
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'geonode_data')
  ->propertyCondition('active', 1);

  $result = $query->execute();

  if (isset($result['geonode_data'])) {
    $ids = array_keys($result['geonode_data']);
    $layers = entity_load('geonode_data', $ids);
  }
  $export = array();

  foreach ($layers as $layer) {
    $openlayers_layers = new stdClass();
    $openlayers_layers->disabled = FALSE; /* Edit this to true to make a default openlayers_layers disabled initially */
    $openlayers_layers->api_version = 1;
    $openlayers_layers->name = $layer->uuid;
    $openlayers_layers->title = $layer->title;
    $openlayers_layers->description = $layer->abstract;
    $openlayers_layers->data = array(
      'base_url' => $layer->wms_server,
      'params' => array(
        'isBaseLayer' => 0,
        'buffer' => '2',
        'ratio' => '1.5',
        'singleTile' => 0,
      ),
      'options' => array(
        'srs' => 'EPSG:900913',
        'TRANSPARENT' => TRUE,
        'exceptions' => 'application/vnd.ogc.se_inimage',
        'format' => 'image/png',
        'layers' => $layer->wms_name,
        'styles' => '',
      ),
      'layer_type' => 'openlayers_layer_type_wms',
    );
    $export[$layer->uuid] = $openlayers_layers;
  }
  return $export;
}