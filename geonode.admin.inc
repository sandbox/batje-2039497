<?php



function geonode_addtaxonomyfield($vid) {
  $prior_field = field_read_field('field_geonode_data_subjects', array('include_inactive' => TRUE));
  if (!empty($prior_field)) {
    // Field exists
    return;
  }

  $vocabularies = taxonomy_get_vocabularies();
  $vocabulary = $vocabularies[$vid];

  $field_config = array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_geonode_data_subjects',
      'field_permissions' => array(
          'type' => '2',
      ),
      'indexes' => array(
          'tid' => array(
              0 => 'tid',
          ),
      ),
      'module' => 'taxonomy',
      'settings' => array(
          'allowed_values' => array(
              0 => array(
                  'vocabulary' => $vocabulary->name,
                  'parent' => '0',
                  'depth' => '',
              ),
          ),
//          'options_list_callback' => 'content_taxonomy_allowed_values',
          'required' => FALSE,
      ),
      'translatable' => '0',
      'type' => 'taxonomy_term_reference',
   );
  field_create_field($field_config);

  $field_instance = array(
      'type' => 'int',
      'label' => t('Subjects'),
      'field_name' => 'field_geonode_data_subjects',
      'entity_type' => 'geonode_data',
      'bundle' => 'geonode_data',
      'widget' => array(
          'active' => 1,
          'module' => 'entityreference',
          'settings' => array(
              'match_operator' => 'CONTAINS',
              'path' => '',
              'size' => '60',
          ),
          'type' => 'entityreference_autocomplete',
      ),
  );

  field_create_instance($field_instance);


}

function geonode_settings_form() {

  $form['geonode'] = array (
      '#type' => 'fieldset',
      '#title' => 'Geonode Servers',
  );
  $form['geonode']['geonode_servers'] = array (
      '#type' => 'textarea',
      '#title' => t('Servers'),
      '#description' => t('Enter the name of the servers (no http). One on each line.'),
      '#size' => 5,
      '#default_value' => variable_get('geonode_servers', ''),
  );


  $options = array('none' => 'None');

  $vids = taxonomy_get_vocabularies();
  foreach($vids as $vid => $value) {
    $options += array($vid => $value->name);
  }


  $form['geonode']['geonode_vocabulary'] = array (
      '#type' => 'select',
      '#title' => t('Subject Vocabulary'),
      '#options' => $options,
      '#default_value' => variable_get('geonode_vocabulary', 0),
      '#required' => TRUE,
  );

  $options = array('none' => 'None');

  $maps = openlayers_maps(FALSE);
  foreach($maps as $map => $value) {
    $options += array($map => $value->title);
  }


  $form['geonode']['geonode_default_map'] = array (
      '#type' => 'select',
      '#title' => t('Default OpenLayers Map'),
      '#options' => $options,
      '#default_value' => variable_get('geonode_default_map', variable_get('openlayers_default_map', 'default')),
      '#required' => FALSE,
      '#description' => t('The Map to use to render layers with.'),
  );



  $form['#submit'][] = 'geonode_settings_form_submit';
  return system_settings_form($form);
}

/**
 * Implements hook_form_validate()
 */
function geonode_settings_form_validate($form, &$form_state) {
  $servers = preg_split( '/\r\n|\r|\n/', $form_state['values']['geonode_servers'] );
  foreach ($servers as $server) {
    $server = trim($server);
    if (!(empty($server))) {
      if (strstr($server, 'http://') === FALSE) {
        form_set_error('Error', 'Server "' . $server . '" does not start with http://');
      }
    }
  }
}

/**
 * Implements hook_form_submit()
 */
function geonode_settings_form_submit($form, &$form_state) {
  if ($form_state['values']['geonode_vocabulary'] != 'none') {
    module_load_include('inc', 'geonode', 'geonode.admin');
    geonode_addtaxonomyfield($form_state['values']['geonode_vocabulary']);
  }
  else {
    //TODO: remove the field if it exists
  }
}
