<?php

/**
 * Task view callback.
 */
function geonode_dataset_view($layer) {
  drupal_set_title(entity_label('geonode_data', $layer));
  return entity_view('geonode_data', array(entity_id('geonode_data', $layer) => $layer), 'full');
}
