<?php

/**
 * Implementing hook_menu().
 */
function geonode_menu() {
  $items['admin/structure/openlayers/geonode'] = array(
      'title' => t('Geonode'),
      'description' => t('Set general Geonode Settings.'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('geonode_settings_form'),
      'access arguments' => array('administer site configuration'),
      'file' => 'geonode.admin.inc',
      'type' => MENU_LOCAL_TASK,
      'weight' => 101,
  );
  $items['admin/structure/openlayers/geonode/settings'] = array(
      'title' => t('Settings'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -5,
  );
  $items['admin/structure/openlayers/geonode/import'] = array(
      'title' => t('Import'),
      'description' => t('Import.'),
      'page callback' => 'geonode_import_all',
      'access arguments' => array('administer site configuration'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 5,
  );


  $task_uri = 'geonode/%geonode_data';
  $task_uri_argument_position = 1;

  $items[$task_uri] = array(
      'title callback' => 'entity_label',
      'title arguments' => array('geonode_data', $task_uri_argument_position),
      'page callback' => 'geonode_data_view',
      'page arguments' => array($task_uri_argument_position),
      'access arguments' => array('view geonode data'),
      'file' => 'geonode.pages.inc',
  );

  $items[$task_uri . '/view'] = array(
      'title' => 'View',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
  );

  $items[$task_uri . '/edit'] = array(
      'title' => 'Edit',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('geonode_data_form', $task_uri_argument_position),
      'access arguments' => array('administer geonode'),
      'file' => 'includes/geonode.forms.inc',
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );

/*  $items['admin/structure/geonode-data/manage/geonode-data'] = array(
    'title' => 'Edit Geonode Data type',
    'access arguments' => array('administer geonode'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('geonode_bundles_form'), //same hook as above, also passing %bundle_key
    'type' => MENU_CALLBACK,
    'file' => 'includes/geonode.forms.inc',
  );
  $items['admin/structure/geonode-data/manage/geonode_data/edit'] = array( // As above.
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK, // >>> TODO: This tab doesn't display <<<
      'weight' => -10,
  );*/
  return $items;
}




/**
 * Implement hook_entity_info().
 *
 * We define the geonode_data entity here
 */
function geonode_entity_info() {
  $return['geonode_data'] = array(
      'label' => t('Geonode Dataset'),
      'entity class' => 'GeonodeData',
      'controller class' => 'GeonodeDataController',
      'base table' => 'geonode_data',
      'fieldable' => TRUE,
      'entity keys' => array(
          'id' => 'id',
          'label' => 'title',
      ),
      'bundles' => array(
        'geonode_data' => array(
            'label' => 'Geonode Data',
            'admin' => array(

            'path' => 'admin/structure/geonode/manage/geonode_data',
            // This is unique to each bundle.
            'real path' => 'admin/structure/geonode/manage',
            'bundle argument' => 4,  // Arg 4 is %bundle_key.
            // What ever permission that you've assigned in hook_permissions.
            'access arguments' => array('administer content types'),
                ),
            ),
          ),
      'bundle keys' => array(
          'bundle' => 'geonode_data',
      ),
      'load hook' => 'geonode_data_load',
      'label callback' => 'entity_class_label',
      'uri callback' => 'entity_class_uri',
//      'creation callback' => 'geonode_access',
      'access callback' => 'geonode_access',
      'module' => 'geonode',
      'view modes' => array(
        'full' => array(
          'label' => t('Full Layer'),
          'custom settings' => FALSE,
        ),
//        'map' => array(
//          'label' => t('Show as Map'),
//          'custom settings' => FALSE,
//        ),
      ),
      // Enable the entity API's admin UI.
      'admin ui' => array(
          'path' => 'admin/structure/geonode/manage',
          'file' => 'includes/geonode_data.entity.inc',
          'controller class' => 'GeonodeDataTypeUIController',
      ),
  );
  return $return;
}

/**
 * Implements hook_entity_property_info_alter().
 */
function geonode_entity_property_info_alter(&$info) {
  $properties = &$info['geonode_data']['properties'];
  $properties['created'] = array(
      'label' => t("Date created"),
      'type' => 'date',
      'description' => t("The date the geonode dataset was posted."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer nodes',
      'schema field' => 'created',
  );
  $properties['changed'] = array(
      'label' => t("Date changed"),
      'type' => 'date',
      'schema field' => 'changed',
      'description' => t("The date the geonode dataset was most recently updated."),
  );
  $properties['uid'] = array(
      'label' => t("Author"),
      'type' => 'user',
      'description' => t("The author of the geonode dataset."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer geonode',
      'required' => TRUE,
      'schema field' => 'uid',
  );
  $properties['uuid'] = array(
      'label' => t("Uuid"),
      'type' => 'text',
      'description' => t("The unique Id of the dataset."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer geonode',
      'required' => TRUE,
      'schema field' => 'uuid',
  );
  $properties['title'] = array(
      'label' => t("Title"),
      'type' => 'text',
      'description' => t("The title of the dataset."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer geonode',
      'required' => TRUE,
      'schema field' => 'title',
  );
  $properties['abstract'] = array(
      'label' => t("Abstract"),
      'type' => 'text',
      'description' => t("The abstract of the dataset."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer geonode',
      'required' => FALSE,
      'schema field' => 'abstract',
  );
  $properties['wms_name'] = array(
      'label' => t("WMS Layer"),
      'type' => 'text',
      'description' => t("The abstract of the dataset."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer geonode',
      'required' => FALSE,
      'schema field' => 'wms_name',
  );
  $properties['wms_server'] = array (
      'label' => t("WMS Server"),
      'type' => 'text',
      'description' => t("The abstract of the dataset."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer geonode',
      'required' => FALSE,
      'schema field' => 'wms_server',
  );
  $properties['detail'] = array (
      'label' => t("Detail page"),
      'type' => 'text',
      'description' => t("Page on geonode with details."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer geonode',
      'required' => FALSE,
      'schema field' => 'detail',
  );
  $properties['download_links'] = array (
      'label' => t("Downloads"),
      'type' => 'struct',
      'description' => t("Page on geonode with details."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer geonode',
      'required' => FALSE,
      'schema field' => 'download_links',
  );
  $properties['metadata_links'] = array (
      'label' => t("Metadata"),
      'type' => 'struct',
      'description' => t("Page on geonode with details."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer geonode',
      'required' => FALSE,
      'schema field' => 'metadata_links',
  );
  $properties['active'] = array (
      'label' => t("Active"),
      'type' => 'integer',
      'description' => t("Page on geonode with details."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer geonode',
      'required' => TRUE,
      'schema field' => 'active',
  );

}

/**
 * Implements hook_permission().
 */
function geonode_permission() {
  // We set up permisssions to manage entity types, manage all entities and the
  // permissions for each individual entity
  $permissions = array(
      'administer geonode' => array(
          'title' => t('Administer Geonode'),
          'description' => t('Administer the Geonode module'),
      ),
      'view geonode data' => array(
          'title' => t('View Geonode Datasets'),
          'description' => t('View all Geonode Datasets'),
      ),
  );

  return $permissions;
}


/**
 * Determines whether the given user has access to a geonode dataset.
 *
 * @param $op
 *   The operation being performed. One of 'view', 'update', 'create', 'delete'
 *   or just 'edit' (being the same as 'create' or 'update').
 * @param $model
 *   Optionally a model or a model type to check access for. If nothing is
 *   given, access for all models is determined.
 * @param $account
 *   The user to check for. Leave it to NULL to check for the global user.
 * @return boolean
 *   Whether access is allowed or not.
 */
function geonode_access($op, $geonode_data = NULL, $account = NULL) {
  if (user_access('administer geonode', $account)) {
    return TRUE;
  }
  if (user_access('view geonode data', $account)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Imports 1 single dataset, where the dataset is the original geonode
 * Dataset array
 *
 * Saves this array as a Drupal entity
 *
 * @param Array $dataset
 * @param string $server The URL of the WMS endpoint
 */
function geonode_import($dataset, $server, &$context) {
  module_load_include('inc', 'geonode', 'includes/geonode_data.entity');
  $uuid = $dataset['uuid'];
  if (!($entity = geonode_data_find_by_uuid($uuid))) {
    $entity = geonode_data_create(Array());
    // Optional message displayed under the progressbar.
    $context['message'] = t("Updating :name", array(":name" => $dataset['title']));
  }
  else {
    // Optional message displayed under the progressbar.
    $context['message'] = t("Creating :name", array(":name" => $dataset['title']));
  }
  $entity->uuid = $uuid;
  $entity->wms_name = $dataset['name'];
  $entity->wms_server = $server;
  $entity->title = $dataset['title'];
  $entity->abstract = $dataset['abstract'];
  $entity->detail = $dataset['detail'];
  $entity->download_links = $dataset['download_links'];

  if ($vid = variable_get('geonode_vocabulary')) {
    $vocabulary = taxonomy_vocabulary_load($vid);
    foreach ($dataset['keywords'] as $subject) {
      if ($term = taxonomy_get_term_by_name($subject, $vocabulary->machine_name)) {
        // not much
        $term = reset($term);
      }
      else {
        $term = new stdClass();
        $term->vid = $vid;
        $term->name = $subject;
        taxonomy_term_save($term);
      }
      $dontload = FALSE;
      if ((isset($entity->field_geonode_data_subjects) && (isset($entity->field_geonode_data_subjects[LANGUAGE_NONE])))) {
        foreach ($entity->field_geonode_data_subjects[LANGUAGE_NONE] as $myterm) {
          if ($myterm['tid'] == $term->tid) {
            $dontload = TRUE;
            break;
          }
        }
      }
      if (!$dontload) {
        $entity->field_geonode_data_subjects[LANGUAGE_NONE][]['tid'] = $term->tid;
      }
    }
  }

  // the bounding box

  if (
      (is_numeric($dataset['bbox']['minx'])) &&
      (is_numeric($dataset['bbox']['miny'])) &&
      (is_numeric($dataset['bbox']['maxx'])) &&
      (is_numeric($dataset['bbox']['maxy']))
      ) {
      $entity->field_geonode_data_bb[LANGUAGE_NONE][0]['wkt'] =
    'LINESTRING(' .
    $dataset['bbox']['minx'] . ' ' . $dataset['bbox']['miny'] . ', ' .
    $dataset['bbox']['minx'] . ' ' . $dataset['bbox']['maxy'] . ', ' .
    $dataset['bbox']['maxx'] . ' ' . $dataset['bbox']['maxy'] . ', ' .
    $dataset['bbox']['maxx'] . ' ' . $dataset['bbox']['miny'] . ', ' .
    $dataset['bbox']['minx'] . ' ' . $dataset['bbox']['miny'] .
    ')';
  }

  $entity->save();

  // Store some results for post-processing in the 'finished' callback.
  // The contents of 'results' will be available as $results in the
  // 'finished' function (in this example, batch_example_finished()).
  $context['results'][] = 'Saved ' . $entity->title;


  return TRUE;
}

/**
 *  Imports all layers from the configured geonode(s)
 */
function geonode_import_all() {
  $servers = preg_split( '/\r\n|\r|\n/', variable_get('geonode_servers'));
  $operations = array();

  $limitjump = 25;

  foreach ($servers as $server) {
    $server = trim($server);
    if (!(empty($server))) {
      $url = $server . "/data/search/api";
      $start = 0;
      $limit = $limitjump;
      $options = $options = array(
        'headers' => array('Content-Type' => 'text/json; charset=UTF-8'),
      );
      while ($limit == $limitjump) {
        $urlfetch = $url . '?start=' . $start . '&limit=' . $limit;

        $json = drupal_http_request($urlfetch);
        $tree = drupal_json_decode($json->data);
        if (isset($tree['rows'])) {
          foreach($tree['rows'] as $key => $value) {
            $operations[] = array('geonode_import', array($value, $server . "/geoserver/wms"));
          }
        }
        $limit = count($tree['rows']);
        $start = $start + $limitjump;
      }
    }
  }
  $batch = array(
      'operations' => $operations,
      'finished' => 'geonode_import_all_finished',
  );
  batch_set($batch);
  batch_process('admin/config/geonode');
}

  /**
   * Batch 'finished' callback
   */
  function geonode_import_all_finished($success, $results, $operations) {
    if ($success) {
      // Here we could do something meaningful with the results.
      // We just display the number of nodes we processed...
      drupal_set_message(t('The final result was "%final"', array('%final' => end($results))));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array('@operation' => $error_operation[0], '@args' => print_r($error_operation[0], TRUE))));
    }
  }




/**
 * Create a model object.
 */
function geonode_data_create($values = array()) {
  return entity_get_controller('geonode_data')->create($values);
}

function geonode_data_save(GeonodeData $geonode_data) {
  return $geonode_data->save();
}

/**
 * Load a geonode dataset.
 */
function geonode_data_load($id) {
  if ($geonode_datas = entity_get_controller('geonode_data')->load(array($id))) {
    $geonode_data = $geonode_datas[$id];
    return $geonode_data;
  }
  else {
    return FALSE;
  }
}

/**
 * Find a geonode dataset
 */
function geonode_data_find_by_uuid($uuid) {

  $query = db_select('geonode_data', 'a')
  ->fields('a', array('id'))
  ->condition('uuid', $uuid);
  if ($id = $query->execute()->fetchField()) {
    return geonode_data_load($id);
  }
  return FALSE;
}



/**
 * Sets up content to show an individual Geonode Dataset
 * @todo - get rid of drupal_set_title();
 */
function geonode_data_view($geonode_data, $view_mode = 'full') {
  $controller = entity_get_controller('geonode_data');
  $content = $controller->view(array($geonode_data->id => $geonode_data));
  drupal_set_title($geonode_data->title);
  return $content;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function geonode_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "openlayers" && $api == "openlayers_layers") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_openlayers_layers().
 */
function geonode_openlayers_layers() {
  module_load_include('inc', 'geonode', 'includes/geonode.openlayers');
  return geonode_get_my_openlayers_layers();
}


/**
 * implements hook_form()
 *
 * Generates the task type editing form.
 */
function geonode_data_form($form, &$form_state, $geonode_data, $op = 'edit') {

  drupal_set_title($geonode_data->title);

  $form['active'] = array(
      '#title' => t('Active'),
      '#type' => 'checkbox',
      '#value' => $geonode_data->active,
      '#description' => t('Is the entity active and visible.'),
  );

  $options = array('none' => 'None');

  $maps = openlayers_maps(FALSE);
  foreach($maps as $map => $value) {
    $options += array($map => $value->name);
  }


  $form['map'] = array (
      '#type' => 'select',
      '#title' => t('OpenLayers Map'),
      '#options' => $options,
      '#default_value' => variable_get('geonode_vocabulary', 0),
      '#required' => FALSE,
      '#description' => t('The Map to use to render this layer.'),
  );




  field_attach_form('geonode_data', $geonode_data, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
      '#weight' => 100,
  );

  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Geonode Data'),
      '#submit' => $submit + array('geonode_data_form_submit'),
  );

  // Show Delete button if we edit task.
  $task_id = entity_id('geonode_data' ,$geonode_data);

  //  $form['#validate'][] = 'geonode_data_form_validate';

  return $form;
}

/**
 * Submit handler for creating/editing task_type.
 */
function geonode_data_form_submit(&$form, &$form_state) {
  $geonode_data = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  geonode_data_save($geonode_data);

  // Redirect user back to list of task types.
  $form_state['redirect'] = 'admin/structure/geonode-data';
}
